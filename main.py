"""
script grr_automator.py
author: rs
This code automates grr forensic analysis
"""
from grr_api_client import api
from grr_api_client import hunt
import sys
import socket
# from db import database
import json
from google.protobuf import json_format
import time
from datetime import datetime
import pprint

def get_ip_address():
    """
    Returns IP address of the machine
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_address = s.getsockname()[0]
    s.close()

    # return "172.20.10.13"
    return ip_address

class GrrAutomator():

    server_address = None
    server_user = None
    server_password = None
    grr_api = None
    db = None
    current_clients = []

    default_admin_port = "8000"


    default_client_rate = 2
    default_client_limit = 1
    default_expiry_time = 300
    all_client_information = {}

    def __init__(self, server_link=None,user=None,password=None):
        """
        gets default ip address
        """
        if server_link is None:
            server_link = "http://{}:{}".format(get_ip_address(), self.default_admin_port)

        # print(server_link)
        if server_link is None or user is None or password is None:
            raise ValueError("server_link, user and password needs to be provided")

        self.server_address = server_link
        self.server_user = user
        self.server_password = password


        self.grr_api = api.InitHttp(api_endpoint=self.server_address,
                              auth=(self.server_user, self.server_password))
        # db = database()

    def find_client(self, query=None):
        """
        query = "host:name"
        """
        # find all clients
        if query is None:
            query = "."

        search_result = self.grr_api.SearchClients(query)

        client_information = {}
        for client in search_result:

            # print(client.__dict__)

            user_info = get_user_data(client.data.knowledge_base)
            # print(user_info)
            client_os = str(client.data.os_info.system)
            client_os_release = str(client.data.os_info.release)
            client_id = str(client.client_id)
            client_last_seen_at = str(client.data.last_seen_at)
            last_booted_at = str(client.data.last_booted_at)


            self.current_clients.append(client_id)

            client_information["client_os"] = client_os
            client_information["client_os_release"] = client_os_release
            client_information["client_id"] = client_id
            client_information["client_last_seen_at"] = client_last_seen_at
            client_information["last_booted_at"] = last_booted_at
            client_information["users"] = user_info



            #
            # client_details = [client_os, client_os_release, client_id]
            self.all_client_information[client_id] = client_information.copy()

            # client_obj = self.grr_api.Client(client_id)

            # for l in client_obj.ListFlows():
            #     print(l.__dict__)
            # approval_id
            # print(client_obj._context.connector.__dict__)


        return self.all_client_information

    def get_all_clients(self):
        """
        Return all clients
        """
        if not self.current_clients:
            self.find_client()
        return self.current_clients

    def create_flow(self, client_id=None, flow_name="ListProcesses"):
        """
        Create single flow
        """
        if client_id is None:
            raise ValueError("No client id provided")

        client_obj = self.grr_api.Client(client_id)
        # flowid = client_obj.CreateFlow(name=flow_name).flow_id
        flow = client_obj.CreateFlow(name=flow_name)
        flowid = flow.flow_id
        # flow.WaitUntilDone()
        # self.PrintFlowResultJson(flow.ListResults())

        self.wait_for_flow(client_obj, flow)

        print(flow)

    def create_browser_history_flow(self, client_id):
        if client_id is None:
            raise ValueError("No client id provided")

        client_obj = self.grr_api.Client(client_id)
        # flowid = client_obj.CreateFlow(name=flow_name).flow_id

        args = self.grr_api.types.CreateFlowArgs("ArtifactCollectorFlow")
        # print(flow)
        args.artifact_list.append("FirefoxHistory")

        flow = client_obj.CreateFlow(name="ArtifactCollectorFlow", args=args)
        flowid = flow.flow_id

        if(self.wait_for_flow(client_obj, flow)):
            #print(flow)
            # self.PrintFlowResultJson(flow.ListResults())
            # self.find_suspicyious_sites(flow.ListResults())

            return flow.ListResults()

        else:
            print("didn't finish")


    def create_netstat_flow(self, client_id):
        if client_id is None:
            raise ValueError("No client id provided")

        client_obj = self.grr_api.Client(client_id)
        # flowid = client_obj.CreateFlow(name=flow_name).flow_id

        # args = self.grr_api.types.CreateFlowArgs("Netstat")
        # print(flow)
        # args.artifact_list.append("ChromeHistory")

        flow = client_obj.CreateFlow(name="Netstat")

        if(self.wait_for_flow(client_obj, flow)):
            #print(flow)
            # self.PrintFlowResultJson(flow.ListResults())
            return flow.ListResults()
        else:
            return False

    def create_browser_history_flow2(self, client_id):
        if client_id is None:
            raise ValueError("No client id provided")

        client_obj = self.grr_api.Client(client_id)
        # flowid = client_obj.CreateFlow(name=flow_name).flow_id

        args = self.grr_api.types.CreateFlowArgs("FirefoxHistory")
        # print(flow
        args.username = "username"
        # args.historyPath = ["C:\Users\<username>\AppData\Local\Google\Chrome\User Data\Default\History"]

        flow = client_obj.CreateFlow(name="ChromeHistory", args=args)
        flowid = flow.flow_id

        if (self.wait_for_flow(client_obj, flow)):
            print(flow)
        else:
            print("didn't finish")

    def PrintFlowResultJson(self, resultList):

        data = None
        print(resultList)
        for result in resultList:
            data = json.loads(json_format.MessageToJson(result.payload))

            print(data)

    def wait_for_flow(self, client, flow):

        flowid = flow.flow_id
        timeout = 180
        timeoutcounter = 0
        flowWorked = True

        print(flow.data.state)
        # time.sleep(70)
        process_line = ""
        while (flow.data.state == flow.data.RUNNING):

            wait_timer(60)

            # flow = self.grr_api.Client(client.client_id).Flow(flowID).Get()
            flow = client.Flow(flowid).Get()
            timeoutcounter += 1
            if (timeoutcounter == timeout):
                client.Flow(flowid).Cancel()
                flowWorked = False

        return flowWorked

    def create_flows_for_all_clients(self, flow_name="FileFinder"):
        """
        Creates flows for all clients
        Default flow name = FileFinder
        """
        main_clients = self.get_all_clients()

        for client_id in ids:
            self.create_flow(client_id=client_id, flow_name=flow_name)

    def create_hunt(self,client_rate=None,client_limit=None,expiry_time=None,flow_name="ArtifactCollectorFlow",artifact_list=["WindowsEventLogs"]):
        """
        Creates hunt
        artifact_list needs to be an array
        """
        if flow_name is None:
            raise ValueError("No flow_name provided")


        if client_rate is not None:
            self.client_rate = client_rate

        if client_limit is not None:
            self.client_limit = client_limit

        if expiry_time is not None:
            self.expiry_time = expiry_time

        hunt_runner_args = self.grr_api.types.CreateHuntRunnerArgs()

        hunt_runner_args.client_rate = self.default_client_rate
        hunt_runner_args.client_limit = self.default_client_limit
        hunt_runner_args.expiry_time = self.default_expiry_time

        rule = hunt_runner_args.client_rule_set.rules.add()


        flow_args = self.grr_api.types.CreateFlowArgs(flow_name)

        if flow_name == "ArtifactCollectorFlow":

            if len(artifact_list) == 0:
                raise ValueError("No artifacts provided")

            for art in artifact_list:
                flow_args.artifact_list.append(art)

        hunt = self.grr_api.CreateHunt(flow_name=flow_name, flow_args=flow_args,
                                 hunt_runner_args=hunt_runner_args)
        hunt = hunt.Start()

        # get reference id
        return hunt

    def GetFlowsForClient(self, client_id):
        return self.GetClient(client_id).ListFlows()
        # for f in self.GetClient(client_id).ListFlows():
        #     #data = json.loads(json_format.MessageToJson(f.))
        #     print(f.flow_id)

    def GetClient(self, client_id):
        return self.grr_api.Client(client_id)

    def GetFlow(self, client_id, flow_id):
        return self.grr_api.Client(client_id).Flow(flow_id)

    # def GetPendingFlows(self, client_id):
    #     self.db = database()
    #     for f in self.db.GetPendingFlows():
    #         flow = self.GetFlow(client_id, f[0])
    #         print(flow.ListResults())

    # def PrintFlowResultJson(client_id, flow_id):
    #     flow = GetClient(client_id).Flow(flow_id)
    #
    #     for result in flow.Get().ListResults():
    #         data = json.loads(json_format.MessageToJson(result.payload))
    #         pprint(data["processName"])

    def find_suspicious_sites(self, resultList):
        sites = ["www.glassdoor.com", "www.linkedin.com", "rit.joinhandshake.com"]
        count = 0
        print(resultList.items)

        for result in resultList:
            print(result)
            data = json.loads(json_format.MessageToJson(result.payload))
            # print(data)
            url = data["domain"]
            print(url)

            if(url in sites):
                ++count;


        print("Mr. Mark has visited suspicious sites %s times" % count)






import sys
import time

def wait_timer(n):
    time_counter = 0
    while True:
        time_counter += 1
        if time_counter <= n:
            time.sleep(1)
            sys.stdout.write("*")
            sys.stdout.flush()
        else:
            break
    sys.stdout.write("\n")



def get_user_data(obj):

    # result_list = []

    required_labels = {"username":None,
                    "last_logon":None,
                    "full_name":None
                    # "os":None,
                    # "os_major_version":None
                    }

    result = {}
    for descriptor in obj.DESCRIPTOR.fields:
        value = getattr(obj, descriptor.name)
        if descriptor.type == descriptor.TYPE_MESSAGE:
            if descriptor.label == descriptor.LABEL_REPEATED:
                new_result = map(get_user_data, value)
                if len(new_result) > 0:
                    # result = merge_two_dicts(result, new_result)
                    return new_result
                    # result_list += new_result
            else:
                new_result = get_user_data(value)


        elif descriptor.type == descriptor.TYPE_ENUM:

            enum_name = descriptor.enum_type.values[value].name
            if descriptor.name in required_labels:
                result[str(descriptor.name)] = str(enum_name)

        else:
            # print "%s: %s" % (descriptor.name, value)
            if descriptor.name in required_labels:
                # if descriptor.name == "last_logon":
                #     # print(value)
                #     # if value != 0:
                #     #     value = datetime.utcfromtimestamp(int(str(value)[:-6])).strftime('%Y-%m-%d %H:%M:%S')
                # print "%s: %s" % (descriptor.name, value)

                result[str(descriptor.name)] = str(value)

        # if result:
        #     result_list.append(result)


        # print(result_list)
        #


    # print(result)
    return result

def get_readable_time(tstamp):
    """
    Converts to date readable
    """

    if tstamp != "0":
        tstamp = datetime.utcfromtimestamp(int(str(tstamp)[:-6])).strftime('%Y-%m-%d %H:%M:%S')
    else:
        tstamp = "Not available"

    return tstamp

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z


def client_summary(client_info):
    """
    """


    operating_sys_counter = 0

    operating_systems_dict = {}

    clients_seen_at = []

    for client_id in client_info:
        client_data = client_info[client_id]
        operating_sys = client_data['client_os']
        operating_sys_release = client_data['client_os_release']

        client_last_seen_at = client_data["last_booted_at"]

        if client_last_seen_at != "0":
            clients_seen_at.append(client_last_seen_at)


        os_name = operating_sys + " " + operating_sys_release

        if os_name in operating_systems_dict:
            operating_systems_dict[os_name] += 1
        else:
            operating_sys_counter += 1
            operating_systems_dict[os_name] = 1

        # pprint.pprint(client_data)

    # print(clients_seen_at)
    latest_client = get_readable_time(max(clients_seen_at))
    oldest_client = get_readable_time(min(clients_seen_at))

    summary_data = {"operating_sys_count":operating_sys_counter,
                    "total_clients":len(client_info),
                    "operating_systems":operating_systems_dict,
                    "latest_client":latest_client,
                    "oldest_client":oldest_client
                    }

    return summary_data


def italic(text):
    return "{}{}{}".format("\x1B[3m",text,"\x1B[23m")


def get_browswer_module(cid):
    """
    Use browser module
    """

    # sites = ["www.glassdoor.com", "www.linkedin.com", "rit.joinhandshake.com"]

    url_input = raw_input('Enter the urls you need to search for: (, to separate)')

    url_array = url_input.split(',')

    response = grr.create_browser_history_flow(cid)

    for r in response:
        data = json.loads(json_format.MessageToJson(r.data.payload))
        domain = data['domain']

        if any(ext in domain for ext in url_array):
            access_time = data['accessTime']
            access_time = get_readable_time(access_time)

            print("{} accessed on {}".format(domain, access_time))




def get_processes_module(cid):
    """
    """

    service_input = raw_input('Enter the services you need to search for: (, to separate) example: ssh,mysql, firefox ')

    service_array = service_input.split(',')

    response = grr.create_netstat_flow(cid)

    running_processes = []
    for r in response:

        data = json.loads(json_format.MessageToJson(r.data.payload))

        process_name = data["processName"]
        if any(ext in process_name for ext in service_array):
            print("{} was running".format(process_name))

    # running_processes.append()
    #
    # uniq_processes = list(set(running_processes))
    #
    # print("These processes were running on user's machine:")
    # for u in uniq_processes:
    #     print(u)
    print("----------------------")


def get_log_in_module(client_information):
    """
    """

    counter = 1
    for client_id in client_information:
        client = client_information[client_id]

        print("-"*100)
        print("Machine #{} ({}) OS: {}".format(counter, client_id, client['client_os']))

        print("**Users | Log On time**")

        user_counter = 1
        for user in client['users']:
            print("{} : {} logged in at {}".format(user_counter, user['username'],get_readable_time(user['last_logon'])))

            user_counter += 1

        counter += 1


if __name__ == "__main__":
    # grr = GrrAutomator(user="admin",password="adminpw")
    grr = GrrAutomator(user="gray",password="gray")
    #print(grr.find_client())
    all_clients = grr.get_all_clients()
    summary_dict = client_summary(grr.all_client_information)

    print("\""*100)
    print(" "*40 + "GRR Forensic Automation".upper())
    print(" "*35 + "(Authors: Arpan, Julio, Rafid, Swathi)")


    print(" "*35 + italic("Woah. Damn professional!") + " - Ms. Ramji")
    print(" "*35 + italic("Looks dapper") + " - Mr. Sarkar")
    print(" "*35 + italic("Looks  good!") + " - Mr. Cesar")
    print(" "*35 + italic("I like it. I like it!") + " - Mr. Saad")

    print("\""*100)
    print(" "*40 + "Summary")
    print("Total Clients {}".format(summary_dict["total_clients"]))
    print("Unique OS Count {}".format(summary_dict["operating_sys_count"]))
    print("Total Clients {}".format(summary_dict["operating_systems"]))
    print("Total Clients {}".format(summary_dict["operating_systems"]))
    print("Latest Boot Time : {}".format(summary_dict["latest_client"]))
    print("Oldest Boot Time : {}".format(summary_dict["oldest_client"]))


    print("\""*100)
    print(" "*40 + "GRR User Interface".upper())
    print("\""*100)
    print("1) To find log in data, type 1")
    print("2) To find flight risk users (browser history), type 2")
    print("3) To find running services, type 3")
    print("To exit type 'exit'")


    user_here = True
    while user_here:
        type = raw_input("Please enter the type value:")


        if str(type) == "2":
            cid = "C.a1499128e412b1c9"
            get_browswer_module(cid)
        elif str(type) == "1":
            get_log_in_module(grr.all_client_information)
        elif str(type) == "3":
            cid = "C.a1499128e412b1c9"
            get_processes_module(cid)
        elif str(type) == "exit":
            user_here = False
            print("Thanks for using GRR Automator")


    # for cid in all_clients:
    #     grr.create_netstat_flow(cid)


    #
    # #grr.create_hunt()
    #
    # for client in all_clients:
    #     client_id = "C.87c98484629c22b4"
    #     # grr.create_flow(client_id)
    #     grr.create_browser_history_flow("C.6f1627bc632842ea")
        # grr.create_netstat_flow("C.6f1627bc632842ea")

        # grr.GetPendingFlows(client_id)